import axios from 'axios'
    import React, { Component } from 'react'
    import { Link } from 'react-router-dom'

    class CategoriesList extends Component {
      constructor () {
        super()
        this.state = {
          categories: [],
          prices: []
        }
      }

      componentDidMount () {
        axios.get('/api/categories').then(response => {
          this.setState({
            categories: response.data
          })
        });

        axios.get('/api/roundprices').then(response => {
          this.setState({
            prices: response.data
          })
        });
      }

      render () {
        const { categories } = this.state
        const { prices } = this.state
        return (

          <div id="categoriesList" className='container-fluid'>
            <div className='row h-100 no-gutters'>
            {categories.map(category => (
              <div class="col-sm align-items-center category-block">
                <div class="col-12 category-block-element">
                  <span class="category">{category.name}</span>
                </div>
                {category.questions.map((question, index) => (
                  <div class="col-12 category-block-element" key={index}>
                    <span class="price ">{prices[index]}</span>
                    <div class="question-block-element">
                      <span>{question.text}</span>
                    </div>
                  </div>
                ))}

              </div>
            ))}
            </div>
          </div>
        )
      }
    }

    export default CategoriesList

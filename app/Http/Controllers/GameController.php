<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class GameController extends Controller
{
    public function index() {

      $questions = Category::with(['questions'])
                             ->limit(6)
                             ->get();
      return $questions->toJson();
    }

    public function roundprices() {

      $currentRound = 1;

      if ($currentRound == 1) {
        $prices = array(100, 150, 200, 250, 300);
      } elseif ($currentRound == 2) {
        $prices = array(200, 300, 400, 500, 600);
      } elseif ($currentRound == 3) {
        $prices = array(0, 0, 0, 0, 0);
      } else {
        $prices = array(0, 0, 0, 0, 0);
      }

      return $prices;

    }
}
